package rc.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "hotelEntityManagerFactory",
                        transactionManagerRef = "hotelTransactionManager",
                        basePackages = {"rc.repository"})
public class HotelDbConfig {

    @Autowired
    private Environment env;

    @Bean(name = "hotelDataSource")
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource customDataSource() {

/*        return DataSourceBuilder.create()
                .driverClassName(env.getProperty("hoteles.datasource.driver-class-name"))
                .url(env.getProperty("hoteles.datasource.url"))
                .username(env.getProperty("hoteles.datasource.username"))
                .password(env.getProperty("hoteles.datasource.password"))
                .build();*/

        return DataSourceBuilder.create().build();
    }

    @Bean(name = "hotelEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,
                                                                       @Qualifier("hotelDataSource") DataSource dataSource) {

        return builder.dataSource(dataSource).packages("rc.domain")
                .persistenceUnit("hotel").build();
    }

    @Bean(name = "hotelTransactionManager")
    public PlatformTransactionManager transactionManager(
            @Qualifier("hotelEntityManagerFactory") EntityManagerFactory entityManagerFactory) {

        return new JpaTransactionManager(entityManagerFactory);
    }
}
