package rc.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import rc.domain.Hotel;


@Repository
public interface HotelRepository extends CrudRepository<Hotel, Long> {
}
