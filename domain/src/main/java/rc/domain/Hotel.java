package rc.domain;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.PropertySource;

import javax.persistence.*;

@Entity(name = "hotel")
@Table(name = "hotel")
@Getter
@Setter
@RequiredArgsConstructor
@PropertySource("classpath:hotels.properties")
public class Hotel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String name;
    private Integer clasfication;
    private boolean isOpen;
}
