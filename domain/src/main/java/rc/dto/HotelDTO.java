package rc.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@RequiredArgsConstructor
@ToString
@ApiModel(value = "management.HotelDTO", description = "DTO for the Hotel entity")
public class HotelDTO {

    private Long id;
    private String name;
    private Integer clasfication;
    private boolean isOpen;
}
