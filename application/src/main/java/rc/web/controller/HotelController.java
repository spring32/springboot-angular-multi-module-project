package rc.web.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rc.domain.Hotel;
import rc.dto.HotelDTO;
import rc.persistence.HotelRepository;
import rc.service.api.HotelService;

import java.util.List;

@RestController
@Api(value = "HotelApi")
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1")
public class HotelController {

    @Autowired
    private HotelService hotelService;

    public HotelController(HotelService hotelService) {
        this.hotelService = hotelService;
    }

    @ApiOperation(value = "Retrieve all the hotels",
            httpMethod = "GET",
            consumes = "application/json",
            produces = "application/json",
            response = HotelDTO.class)
    @GetMapping(value = "/hotels")
    public List<HotelDTO> getHotels() {
        return this.hotelService.getAll();
    }
}
