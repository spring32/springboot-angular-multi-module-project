package rc.service.api;

import rc.dto.HotelDTO;
import rc.exception.ResourceNotFoundException;

import java.util.List;
import java.util.Map;

/**
 * This interface provides the service related
 * to a hotel, such as <b>find</b> and <b>create</b>.
 *
 * To be implemented.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
public interface HotelService {

    /**
     * To get all the holtes.
     * @return An employees list
     */
    List<HotelDTO> getAll();

    /**
     * Finds the hotel with the provided hotel id.
     * @param hotelId The hotel id
     * @return The hotel
     * @throws ResourceNotFoundException If the resource doesn't exists.
     */
    HotelDTO getHotelById(Long hotelId) throws ResourceNotFoundException;

    /**
     * Creates a new hotel.
     * @param hotel
     * @return The hotel created.
     */
    HotelDTO createHotel(HotelDTO hotel);

    /**
     * Update an hotel.
     * @param hotelId The hotel id.
     * @param hotelDetails The hotel data to be updated.
     * @return The hotel updated.
     * @throws ResourceNotFoundException If the resource doesn't exists.
     */
    HotelDTO updateHotel(Long hotelId, HotelDTO hotelDetails) throws ResourceNotFoundException;

    /**
     * Deletes an hotel.
     * @param hotelId The hotel id.
     * @return
     * @throws ResourceNotFoundException If the resource doesn't exists.
     */
    Map<String, Boolean> deleteHotel(Long hotelId) throws ResourceNotFoundException;
}
