package rc.service.api.impl;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import rc.domain.Hotel;
import rc.dto.HotelDTO;
import rc.exception.ResourceNotFoundException;
import rc.persistence.HotelRepository;
import rc.service.api.HotelService;
import rc.service.api.impl.utils.ModelMapperUtil;

import javax.persistence.EntityExistsException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

@Service
@Transactional(
        isolation = Isolation.READ_COMMITTED,
        propagation = Propagation.SUPPORTS,
        timeout = 30
)
public class HotelServiceImpl implements HotelService {

    @Autowired
    HotelRepository hotelRepository;

    private ModelMapper modelMapper = new ModelMapper();

    /**
     * To get all the holtes.
     * @return An employees list
     */
    @Override
    @Transactional(readOnly = true)
    public List<HotelDTO> getAll() {
        List<Hotel> hotels = (List<Hotel>) hotelRepository.findAll();

        // Conversion type
        Type listType = new TypeToken<List<HotelDTO>>() {}.getType();

        return (List<HotelDTO>) ModelMapperUtil.mappingDomain2Dto(hotels, listType);
    }

    /**
     * Finds the hotel with the provided hotel id.
     * @param hotelId The hotel id
     * @return The hotel
     * @throws ResourceNotFoundException If the resource doesn't exists.
     */
    @Override
    @Transactional(readOnly = true)
    public HotelDTO getHotelById(Long hotelId) throws ResourceNotFoundException {
        Hotel hotel = hotelRepository.findById(hotelId)
                .orElseThrow(() -> new ResourceNotFoundException("Hotel not found for this id: " +
                        hotelId));

        return (HotelDTO) ModelMapperUtil.mappingDomain2Dto(hotel, HotelDTO.class);
    }

    /**
     * Creates a new hotel.
     * @param hotel
     * @return The hotel created.
     */
    @Override
    @Transactional(
            propagation = Propagation.REQUIRES_NEW,
            rollbackFor = Exception.class,
            noRollbackFor = ResourceNotFoundException.class)
    public HotelDTO createHotel(HotelDTO hotel) {
        Hotel storedHotel = hotelRepository.save((
                Hotel) ModelMapperUtil.mappingDomain2Dto(hotel, Hotel.class));

        return (HotelDTO) ModelMapperUtil.mappingDomain2Dto(storedHotel, HotelDTO.class);
    }

    /**
     * Update an hotel.
     * @param hotelId The hotel id.
     * @param hotelDetails The hotel data to be updated.
     * @return The hotel updated.
     * @throws ResourceNotFoundException If the resource doesn't exists.
     */
    @Override
    @Transactional(
            propagation = Propagation.REQUIRES_NEW,
            rollbackFor = Exception.class,
            noRollbackFor = ResourceNotFoundException.class)
    public HotelDTO updateHotel(Long hotelId, HotelDTO hotelDetails) throws ResourceNotFoundException {
        return null;
    }

    /**
     * Deletes an hotel.
     * @param hotelId The hotel id.
     * @return
     * @throws ResourceNotFoundException If the resource doesn't exists.
     */
    @Override
    @Transactional(
            propagation = Propagation.REQUIRES_NEW,
            rollbackFor = {Exception.class, ResourceNotFoundException.class})
    public Map<String, Boolean> deleteHotel(Long hotelId) throws ResourceNotFoundException {
        return null;
    }
}
