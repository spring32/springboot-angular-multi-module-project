package rc.service.api.impl.utils;

import org.modelmapper.ModelMapper;

import java.lang.reflect.Type;

public class ModelMapperUtil {

    static ModelMapper modelMapper = new ModelMapper();

    public static Object mappingDomain2Dto(Object source, Type destinationType) {

        return modelMapper.map(source, destinationType);
    }
}
